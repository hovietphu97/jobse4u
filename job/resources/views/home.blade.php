@extends('layouts.theme')

@section('content')
    
@php
$user = Auth::user();
@endphp
    <div class="container">
        <ul style="display: inline-block;" class="nav">
            <?php if(Auth::check()){ ?>
                <!-- student -->
            @if($user->is_user())
            <div class="dropdown">
                <li class="nav-item" style="display: inline-block;"><a class="nav-link"  href="{{route('applied_jobs')}}" class="list-group-item-action active"><span class="sidebar-icon"><i class="la la-list-alt"></i> </span>Applies Jobs</a></li>
                <!-- <li class="nav-item" style="display: inline-block;"><a class="nav-link"  href="{{route('create_new_cv')}}">Create CV</a></li>
                <li class="nav-item" style="display: inline-block;"><a class="nav-link"  href="{{ route('view_cv' , [Auth::user()->id]) }}">My CV</a></li> -->
                <li class="nav-item" style="display: inline-block;"><a class="nav-link"  href="{{route('profile')}}"><span class="sidebar-icon"><i class="la la-user"></i> </span>My Profile</a></li>
                <li class="nav-item" style="display: inline-block;"><a class="nav-link"  href="{{route('change_password')}}"><span class="sidebar-icon"><i class="la la-lock"></i> </span>Change Password</a></li>
                <a href="" style="color:#38c172; background: none; border:none;" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span ><i class="la la-clipboard"></i> </span>
                    <span >Curriculum Vitae</span>
                </a>
                <ul class="dropdown-menu" >
                    <li class="nav-item" ><a class="dropdown-item sidebar-link " href="{{route('create_new_cv')}}">Create CV</a></li>
                    <li class="nav-item" ><a class="dropdown-item sidebar-link" href="{{ route('view_cv' , [Auth::user()->id]) }}">My CV</a></li>
                </ul>
            </div>
            @endif
            <!-- company -->
            @if($user->is_employer())
            <div class="dropdown">

                <li class="nav-item" style="display: inline-block;"><a class="nav-link"  href="{{route('profile')}}"><span class="sidebar-icon"><i class="la la-user"></i> </span>My Profile</a></li>
                <li class="nav-item" style="display: inline-block;"><a class="nav-link"  href="{{route('change_password')}}"><span class="sidebar-icon"><i class="la la-lock"></i> </span>Change Password</a></li>


                <a href="" style="color:#38c172; background: none; border:none;" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span class="sidebar-icon"><i class="la la-black-tie"></i> </span>
                    <span class="title">@lang('app.employer')</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-item"><a class="dropdown-item sidebar-link" href="{{route('post_new_job')}}">@lang('app.post_new_job')</a></li>
                    <li class="nav-item"><a class="dropdown-item sidebar-link" href="{{route('posted_jobs' )}}">@lang('app.posted_jobs')</a></li>
                    <li class="nav-item"><a class="dropdown-item sidebar-link" href="{{route('employer_applicant')}}">@lang('app.applicants')</a></li>
                    <li class="nav-item"><a class="dropdown-item sidebar-link" href="{{route('shortlisted_applicant')}}">@lang('app.shortlist')</a></li>
                    <li class="nav-item"><a class="dropdown-item sidebar-link" href="{{route('employer_profile')}}">@lang('app.profile')</a></li>
                </ul>
            </div>



            @endif

            <?php } ?>
            
        </ul>
    </div>
    <div class="premium-jobs-wrap pb-5 pt-5">

          

        </div>
    <div class="home-hero-section">



        <div class="job-search-bar">

            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>Find the job that you deserve</h1>
                        <p class="mt-4 mb-4 job-search-sub-text">More than 3000+ trusted live jobs available from 500+ different employer, <br /> and agents on this website to take your career next level</p>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-md-12">

                        <form action="{{route('jobs_listing')}}" class="form-inline" method="get">
                            <div class="form-row">
                                <div class="col-auto">
                                    <input type="text" name="q" class="form-control mb-2" style="min-width: 300px;" placeholder="@lang('app.job_title_placeholder')">
                                    <input type="text" name="location" class="form-control" style="min-width: 300px;"  placeholder="@lang('app.job_location_placeholder')">
                                    <button type="submit" class="btn btn-success mb-2"><i class="la la-search"></i> @lang('app.search') @lang('app.job')</button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

        </div>

    </div>

    @if($categories->count())
        <div class="home-categories-wrap bg-white pb-5 pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-3">@lang('app.browse_category')</h4>
                    </div>
                </div>

                <div class="row">
                    @foreach($categories as $category)
                        <div class="col-md-4">

                            <p>
                                <a href="{{route('jobs_listing', ['category' => $category->id])}}" class="category-link"><i class="la la-th-large"></i> {{$category->category_name}} <span class="text-muted">({{$category->job_count}})</span> </a>
                            </p>

                        </div>

                    @endforeach

                </div>

            </div>
        </div>
    @endif
    <div class="premium-jobs-wrap pb-5 pt-5" style="max-width: 1800px; margin: 0 50px;">
        <div class="row">
            <div class="col-md-6">
                @if(Auth::check() && $user->is_user())
                @if($quick_job->count())
                    <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="mb-3">@lang('app.quick_jobs')</h4>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    @foreach($quick_job as $arr)
                            @foreach($arr as $job)
                        <div class="col-md-6 mb-3">
                            <div class="premium-job-box p-3 bg-white box-shadow">

                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="premium-job-logo">
                                            <a href="{{route('jobs_by_employer', $job->employer->company_slug)}}">
                                                <img src="{{$job->employer->logo_url}}" class="img-fluid" />
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-sm-6">

                                        <p class="job-title">
                                            <a href="{{route('job_view', $job->id)}}">{!! $job->job_title !!}</a>
                                        </p>

                                        <p class="text-muted m-0">
                                            <a href="{{route('jobs_by_employer', $job->employer->company_slug)}}" class="text-muted">
                                                {{$job->employer->company}}
                                            </a>
                                        </p>

                                        <p class="text-muted m-0">
                                            <i class="la la-map-marker"></i>
                                            @if($job->city_name)
                                                {!! $job->city_name !!},
                                            @endif
                                            @if($job->state_name)
                                                {!! $job->state_name !!},
                                            @endif
                                            @if($job->state_name)
                                                {!! $job->country_name !!}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                        @endforeach
                    </div>
                    @else
                    @if(Auth::check() && $user->is_user())
                        <h4>There's not available quick job for you.</h4>
                    @endif
                    @endif

                @endif
            <!-- CV -->
            @if(Auth::check() && $user->is_employer())
                @if($quick_cv->count())
                    <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="mb-3">Quick CV</h4>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    @foreach($quick_cv as $arr)
                            @foreach($arr as $cv)
                        <div class="col-md-6 mb-3">
                            <div class="premium-job-box p-3 bg-white box-shadow">

                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="premium-job-logo">
                                            <a href="{{url('/dashboard/u/cv/view/'.$cv->user_id)}}">
                                            <i class="fas fa-user"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-sm-6">

                                        <p class="job-title">
                                            <a href="{{url('/dashboard/u/cv/view/'.$cv->user_id)}}">{!! $cv->fullname !!}</a>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                        @endforeach
                    </div>
                    @else
                    @if(Auth::check() && $user->is_employer())
                        <h4>There's not available quick cv for you.</h4>
                    @endif
                    @endif

                @endif
            </div>
            <div class="<?= Auth::check() ? 'col-md-6' : 'col-md-12' ?>">
             @if($new_jobs->count())

                <div class="row">
                    <div class="col-md-6">
                        <h4 class="mb-3">@lang('app.new_jobs')</h4>
                    </div>
                </div>

                <div class="row">
                    @foreach($new_jobs as $job)
                        <div class="<?= Auth::check() ? 'col-md-6' : 'col-md-3' ?> mb-3">
                            <div class="premium-job-box p-3 bg-white box-shadow">

                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="premium-job-logo">
                                            <a href="{{route('jobs_by_employer', $job->employer->company_slug)}}">
                                                <img src="{{$job->employer->logo_url}}" class="img-fluid" />
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-md-8 col-sm-6">

                                        <p class="job-title">
                                            <a href="{{route('job_view', $job->id)}}">{!! $job->job_title !!}</a>
                                        </p>

                                        <p class="text-muted m-0">
                                            <a href="{{route('jobs_by_employer', $job->employer->company_slug)}}" class="text-muted">
                                                {{$job->employer->company}}
                                            </a>
                                        </p>

                                        <p class="text-muted m-0">
                                            <i class="la la-map-marker"></i>
                                            @if($job->city_name)
                                                {!! $job->city_name !!},
                                            @endif
                                            @if($job->state_name)
                                                {!! $job->state_name !!},
                                            @endif
                                            @if($job->state_name)
                                                {!! $job->country_name !!}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
                @endif
            </div>
        
            </div>
            </div>
        </div>
        </div>
    </div>


    



    <div class="new-registration-page bg-white pb-5 pt-5">
        <div class="container">
            <div class="row" style="text-align: center">
                <div class="col-md-6">
                    <div class="home-register-account-box">
                        <h4>@lang('app.job_seeker')</h4>
                        <p class="box-icon"><img src="{{asset('assets/images/employee.png')}}" /></p>
                        <p>@lang('app.job_seeker_new_desc')</p>
                        <a href="{{route('register_job_seeker')}}" class="btn btn-success"><i class="la la-user-plus"></i> @lang('app.register_account') </a>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="home-register-account-box">
                        <h4>@lang('app.employer')</h4>
                        <p class="box-icon"><img src="{{asset('assets/images/enterprise.png')}}" /></p>
                        <p>@lang('app.employer_new_desc')</p>
                        <a href="{{route('register_employer')}}" class="btn btn-success"><i class="la la-user-plus"></i> @lang('app.register_account') </a>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <div class="home-blog-section pb-5 pt-5">
        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <div class="pricing-section-heading mb-5 text-center">
                        <h1>From Our Blog</h1>
                        <h5 class="text-muted">Check the latest updates/news from us.</h5>
                    </div>

                </div>
            </div>


            <div class="row">

                @foreach($blog_posts as $post)

                    <div class="col-md-4">

                        <div class="blog-card-wrap bg-white p-3 mb-4">

                            <div class="blog-card-img mb-4">
                                <img src="{{$post->feature_image_thumb_uri}}" class="card-img" />
                            </div>

                            <h4 class="mb-3">{{$post->title}}</h4>

                            <p class="blog-card-text-preview">{!! limit_words($post->post_content) !!}</p>

                            <a href="{{route('blog_post_single', $post->slug)}}" class="btn btn-success"> <i class="la la-book"></i> Read More</a>

                            <div class="blog-card-footer border-top pt-3 mt-3">
                                <span><i class="la la-user"></i> {{$post->author->name}} </span>
                                <span><i class="la la-clock-o"></i> {{$post->created_at->diffForHumans()}} </span>
                                <span><i class="la la-eye"></i> {{$post->views}} </span>
                            </div>
                        </div>


                    </div>

                @endforeach

            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="home-all-blog-posts-btn-wrap text-center my-3">

                        <a href="{{route('blog_index')}}" class="btn btn-success btn-lg"><i class="la la-link"></i> @lang('app.all_blog_posts')</a>

                    </div>
                </div>
            </div>


        </div>
    </div>



    <div class="new-registration-page bg-white pb-5 pt-5">
        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="call-to-action-post-job justify-content-center">
                        <div class="job-post-icon my-auto">
                            <img src="{{asset('assets/images/job.png')}}" />
                        </div>
                        <div class="job-post-details mr-3 ml-3 p-3 my-auto">
                            <h1>Post your job</h1>
                            <p>
                                Job seekers looking for quality job always. <br /> Post your job to get the talents
                            </p>
                        </div>

                        <div class="job-post-button my-auto">
                            <a href="{{route('post_new_job')}}" class="btn btn-success btn-lg">Post a Job</a>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="job-stats-footer pb-5 pt-5 text-center">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-muted mb-3">Our website stats</h2>
                    <p class="text-muted mb-4">Here the stats of how many people we've helped them to find jobs, hired talents</p>

                </div>
            </div>


            <div class="row">
                <div class="col-md-3">
                    <h3>15M</h3>
                    <h5>Job Applicants</h5>
                </div>

                <div class="col-md-3">
                    <h3>12M</h3>
                    <h5>Job Posted</h5>
                </div>
                <div class="col-md-3">
                    <h3>8M</h3>
                    <h5>Employers</h5>
                </div>
                <div class="col-md-3">
                    <h3>15M</h3>
                    <h5>Recruiters</h5>
                </div>
            </div>
        </div>
    </div>

    
@endsection
