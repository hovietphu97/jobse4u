@extends('layouts.theme')

@section('content')
    <div class="blog-single-page bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="blog-single-title mt-5">
                        <h1>Edit your CV</h1>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="blog-single-content pt-3 pb-5">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Fullname</label>
                                <div class="col-md-6">
                                    <input id="fullname" type="text" class="form-control" name="fullname" value="{{$editCV['fullname']}}">
                                    {!! e_form_error('fullname', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Date of birth</label>
                                <div class="col-md-6">
                                    <input id="dayofbirth" type="date" class="form-control" name="dayofbirth" value="{{$editCV['dayofbirth']}}">
                                    {!! e_form_error('dayofbirth', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Education</label>
                                <div class="col-md-6">
                                    <!-- <input id="education" type="text" class="form-control" name="education" value="{{$editCV['education']}}"> -->
                                    <textarea id="education" type="text" class="form-control" name="education">{{$editCV['education']}}</textarea>
                                    {!! e_form_error('education', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Work Experience</label>
                                <div class="col-md-6">
                                   <!--  <input id="working_exp" type="text" class="form-control" name="working_exp" value="{{$editCV['working_exp']}}"> -->
                                    <textarea id="working_exp" type="text" class="form-control" name="working_exp">{{$editCV['working_exp']}}</textarea>
                                    {!! e_form_error('working_exp', $errors) !!}

                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Personal Skill</label>
                                <div class="col-md-6">
                                    <textarea id="personal_skill" type="text" class="form-control" name="personal_skill">{{$editCV['personal_skill']}}</textarea>
                                    {!! e_form_error('personal_skill', $errors) !!}

                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Project participation</label>
                                <div class="col-md-6">
                                    <textarea id="project_participation" type="text" class="form-control" name="project_participation">{{$editCV['project_participation']}}</textarea>
                                    {!! e_form_error('project_participation', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Activities</label>
                                <div class="col-md-6">
                                    <textarea id="activities" type="text" class="form-control" name="activities">{{$editCV['activities']}}</textarea>
                                    {!! e_form_error('activities', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Goal Career</label>
                                <div class="col-md-6">
                                    <textarea id="goal_career" type="text" class="form-control" name="goal_career" >{{$editCV['goal_career']}}</textarea>
                                    {!! e_form_error('goal_career', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Certificate</label>
                                <div class="col-md-6">
                                    <textarea id="certificate" type="text" class="form-control" name="certificate">{{$editCV['certificate']}}</textarea>
                                    {!! e_form_error('certificate', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">My Hobby</label>
                                <div class="col-md-6">
                                    <textarea id="your_hobby" type="text" class="form-control" name="your_hobby">{{$editCV['your_hobby']}}</textarea>
                                    {!! e_form_error('your_hobby', $errors) !!}

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input id="user_id" type="hidden" class="form-control" name="user_id" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 control-label">Category</label>
                                <div class="col-md-6">
                                    <select name="category_id" class="form-control">
                                    @foreach($getCategory as $value)
                                        <option value="{{$value['id']}}">{{$value['category_name']}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-success">
                                        UPDATE CV
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <table id="skill" class=" table order-list">
                            <thead>
                                <tr>
                                    <td>Your skills</td>
                                    <td>progress</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($skill as $key => $value)
                                <tr>

                                    <td>
                                        <input style="width: 300px;" type="text" class="form-control language" name="language[]" value="{{$key}}" />
                                    </td>
                                    <td>
                                        <select style="padding:0;" class="form-control" class="progress" name="progress[]">
                                            <option value = "10">10%</option>
                                            <option value = "20">20%</option>
                                            <option value = "30">30%</option>
                                            <option value = "40">40%</option>
                                            <option value = "50">50%</option>
                                            <option value = "60">60%</option>
                                            <option value = "70">70%</option>
                                            <option value = "80">80%</option>
                                            <option value = "90">90%</option>
                                            <option value = "100">100%</option>
                                        </select>
                                    </td>
                                    <th class="col-sm-2">
                                        <a class="deleteRow"></a>
                                    </th>

                                </tr>
                                @endforeach

                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="5" style="text-align: left;">
                                        <input type="button" class="btn btn-lg btn-block" style="background-color: #38c172;" id="addrow" value="Add Row" />
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type='text/javascript'>
    $(document).ready(function() {
        // $('form').on('submit', function(){
        //     var json = html2json();
        //     console.log($('input.total_skill').val())
        //     //console.log(json);
        //     return false;
        // })
        var counter = 0;

        $("#addrow").on("click", function() {
            var newRow = $("<tr>");
            var cols = "";

            cols += '<td><input style="width: 300px;" type="text" class="form-control" name="language[]"/></td>';
            cols += '<td><select style="padding:0;" class="form-control" name="progress[]">' +
                '<option value = "10">10%</option>' +
                '<option value = "20">20%</option>' +
                '<option value = "30">30%</option>' +
                '<option value = "40">40%</option>' +
                '<option value = "50">50%</option>' +
                '<option value = "60">60%</option>' +
                '<option value = "70">70%</option>' +
                '<option value = "80">80%</option>' +
                '<option value = "90">90%</option>' +
                '<option value = "100">100%</option>' +
                '</select></td>';

            cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
            newRow.append(cols);
            $("table.order-list").append(newRow);
            counter++;
        });



        $("table.order-list").on("click", ".ibtnDel", function(event) {
            $(this).closest("tr").remove();
            counter -= 1
        });


    });

    function calculateRow(row) {
        var price = +row.find('input[name^="price"]').val();

    }

    function calculateGrandTotal() {
        var grandTotal = 0;
        $("table.order-list").find('input[name^="price"]').each(function() {
            grandTotal += +$(this).val();
        });
        $("#grandtotal").text(grandTotal.toFixed(2));
    }
</script>
@endsection
