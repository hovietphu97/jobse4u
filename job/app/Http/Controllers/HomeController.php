<?php

namespace App\Http\Controllers;

use App\Category;
use App\Job;
use App\Mail\ContactUs;
use App\Mail\ContactUsSendToSender;
use App\Post;
use App\Pricing;
use App\CV;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Auth;
use Illuminate\Database\Eloquent\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $quick_job = new Collection();
        $quick_cv = new Collection();
        if(Auth::check()){

            if(Auth::user()->is_user()){
                $id = Auth::user()->id;
                $cv_student = CV::where('user_id',$id)->get()->toArray();
                if(!empty( $cv_student) && $cv_student[0]['personal_skill'] != null){
                    $skill = json_decode($cv_student[0]['personal_skill'], true);
                    foreach ($skill as $key => $value) {
                        if($key !== '') {
                            $job = Job::search($key)->get();
                            if($job->count()) {$quick_job -> push($job);}
                        }
                    }
                }
            }
            if(Auth::user()->is_employer()){
                $id = Auth::user()->id;
                $job_company = Job::where('user_id',$id)->get()->toArray();
                if(!empty( $job_company) && $job_company[0]['skills'] != null){
                    $skill = explode(',',$job_company[0]['skills']);
                    foreach ($skill as $key => $value) {
                        if($value !== '') {
                            $cv = CV::search(ltrim($value))->get();
                            if($cv->count()) {
                                $quick_cv->push($cv);
                            }
                        }
                    }
                                

                }

            }
        }
        $categories = Category::orderBy('category_name', 'asc')->get();
        $new_jobs = Job::active()->orderBy('created_at', 'desc')->with('employer')->orderBy('created_at','desc')->take(10)->get();
        $blog_posts = Post::whereType('post')->with('author')->orderBy('id', 'desc')->take(3)->get();
        return view('home', compact('categories', 'new_jobs', 'blog_posts','quick_job','quick_cv'));
    }

    public function newRegister(){
        $title = __('app.register');
        return view('new_register', compact('title'));
    }

    public function contactUs(){
        $title = trans('app.contact_us');
        return view('contact_us', compact('title'));
    }

    public function contactUsPost(Request $request){
        $rules = [
            'name'  => 'required',
            'email'  => 'required|email',
            'subject'  => 'required',
        ];

        $this->validate($request, $rules);

        try{
            Mail::send(new ContactUs($request));
            Mail::send(new ContactUsSendToSender($request));
        }catch (\Exception $exception){
            return redirect()->back()->with('error', '<h4>'.trans('app.smtp_error_message').'</h4>'. $exception->getMessage());
        }

        return redirect()->back()->with('success', trans('app.message_has_been_sent'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * Clear all cache
     */
    public function clearCache(){
        Artisan::call('debugbar:clear');
        Artisan::call('view:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        Artisan::call('cache:clear');
        if (function_exists('exec')){
            exec('rm ' . storage_path('logs/*'));
        }
        return redirect(route('home'));
    }

}
