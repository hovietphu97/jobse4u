@extends('layouts.theme')

@section('content')
<article class="resume-wrapper text-center position-relative">
	    <div class="resume-wrapper-inner mx-auto text-left bg-white shadow-lg">
	    	@if(!empty($getCV))
		    <header class="resume-header pt-4 pt-md-0">

			    <div style="background: black;" class="media flex-column flex-md-row">
				    <div  class="media-body p-4 d-flex flex-column flex-md-row mx-auto mx-lg-0">
				    	
					    <div  class="primary-info">
						    <h1 class="name mt-0 mb-1 text-white text-uppercase text-uppercase" style="color:white!important;"><i class="fas fa-user-tie"></i> {{$getCV[0]['fullname']}}</h1>
						    <div class="title mb-3" style="color:white!important;"><i class="fas fa-book"></i> {{$getCate[0]['category_name']}}</div>
						    <ul class="list-unstyled">
							    <li class="mb-2"><a style="color:white;"><i class="far fa-envelope"></i> {{$user[0]['email']}}</a></li>
							    <li class="mb-2"><a style="color:white;"><i class="fas fa-birthday-cake"></i> {{$getCV[0]['dayofbirth']}}</a></li>
							    <li class="mb-2"><a style="color:white;"><i class="fas fa-map-marker-alt"></i>  {{$user[0]['address']}}, {{$user[0]['city']}}, {{$user[0]['country_name']}}</a></li>
							    <li class="mb-2"><a style="color:white;"><i class="fas fa-phone"></i> {{$user[0]['phone']}}</a></li>
							    @if(Auth::user()->is_user())
							    	<li class="mb-2"><a style="color:red;" href="{{url('dashboard/u/cv/edit/'.$getCV[0]['id'])}}"><i class="fas fa-edit"></i> EDIT CV</a></li>
							    @endif
						    </ul>
					    </div><!--//primary-info-->

				    </div><!--//media-body-->
			    </div><!--//media-->
		    </header>
		    <div class="resume-body p-5">
			    <section class="resume-section summary-section mb-5">
				    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Work Experience</h2>
				    <div class="resume-section-content">
					    <p class="mb-0">
					    {{$getCV[0]['working_exp']}}
					    </p>
				    </div>
			    </section><!--//summary-section-->
			    <div class="row">
				    <div class="col-lg-9">
					    <!--  -->
					    <section class="resume-section experience-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Project participation</h2>
						    <div class="resume-section-content">
							    <div class="resume-timeline position-relative">
								    <article class="resume-timeline-item position-relative pb-5">
									   
									    <div class="resume-timeline-item-desc">
										    <p>{{$getCV[0]['project_participation']}}</p>
										</div><!--//resume-timeline-item-desc-->
								    </article><!--//resume-timeline-item-->
							    </div><!--//resume-timeline-->
						    </div>
					    </section><!--//experience-section-->
					    <section class="resume-section experience-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Activities</h2>
						    <div class="resume-section-content">
							    <div class="resume-timeline position-relative">
								    <article class="resume-timeline-item position-relative pb-5">
									   
									    <div class="resume-timeline-item-desc">
										    <p>{{$getCV[0]['activities']}}</p>
										</div><!--//resume-timeline-item-desc-->
								    </article><!--//resume-timeline-item-->
							    </div><!--//resume-timeline-->
						    </div>
					    </section><!--//experience-section-->
					      <section class="resume-section experience-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Goal Career</h2>
						    <div class="resume-section-content">
							    <div class="resume-timeline position-relative">
								    <article class="resume-timeline-item position-relative pb-5">
									   
									    <div class="resume-timeline-item-desc">
										    <p>{{$getCV[0]['goal_career']}}</p>
										</div><!--//resume-timeline-item-desc-->
								    </article><!--//resume-timeline-item-->
							    </div><!--//resume-timeline-->
						    </div>
					    </section><!--//experience-section-->
					      <section class="resume-section experience-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Certificate</h2>
						    <div class="resume-section-content">
							    <div class="resume-timeline position-relative">
								    <article class="resume-timeline-item position-relative pb-5">
									   
									    <div class="resume-timeline-item-desc">
										    <p>{{$getCV[0]['certificate']}}</p>
										</div><!--//resume-timeline-item-desc-->
								    </article><!--//resume-timeline-item-->
							    </div><!--//resume-timeline-->
						    </div>
					    </section><!--//experience-section-->

				    </div>
				    <div class="col-lg-3">
					    <section class="resume-section skills-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Skills</h2>
						    <div class="resume-section-content">
						        <div class="resume-skill-item">
							        <ul class="list-unstyled mb-4">
										@foreach($skill as $key => $value)
											@if($key !== '')
												<li class="mb-2">
													<div class="resume-skill-name">{{$key}}</div>
													<div class="progress resume-progress">
														<?php echo "<div class='progress-bar theme-progress-bar-dark' role='progressbar' style='width:".$value."%' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>" ?>
														<!-- <div class="progress-bar theme-progress-bar-dark" role="progressbar" style="width: $value%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div> -->
													</div>
												</li>
											@endif
										@endforeach
							        </ul>
						        </div><!--//resume-skill-item-->
						        
						       <!--  <div class="resume-skill-item">
						            <h4 class="resume-skills-cat font-weight-bold">Others</h4>
						            <ul class="list-inline">
							            <li class="list-inline-item"><span class="badge badge-light">DevOps</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">Code Review</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">Git</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">Unit Testing</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">Wireframing</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">Sketch</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">Balsamiq</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">WordPress</span></li>
							            <li class="list-inline-item"><span class="badge badge-light">Shopify</span></li>
						            </ul>
						        </div>//resume-skill-item-->
						    </div><!--resume-section-content-->
					    </section><!--//skills-section-->
					    <section class="resume-section education-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Education</h2>
						    <div class="resume-section-content">
							   <p>{{$getCV[0]['education']}}</p>

						    </div>
					    </section><!--//education-section-->
					    <!-- <section class="resume-section reference-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Awards</h2>
						    <div class="resume-section-content">
							    <ul class="list-unstyled resume-awards-list">
								    <li class="mb-2 pl-4 position-relative">
								        <i class="resume-award-icon fas fa-trophy position-absolute" data-fa-transform="shrink-2"></i>
								        <div class="resume-award-name">Award Name Lorem</div>
								        <div class="resume-award-desc">Award desc goes here, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo.</div>
								    </li>
								    <li class="mb-0 pl-4 position-relative">
								        <i class="resume-award-icon fas fa-trophy position-absolute" data-fa-transform="shrink-2"></i>
								        <div class="resume-award-name">Award Name Ipsum</div>
								        <div class="resume-award-desc">Award desc goes here, ultricies nec, pellentesque.</div>
								    </li>
							    </ul>
						    </div>
					    </section> --><!--//interests-section-->
					  <!--   <section class="resume-section language-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Language</h2>
						    <div class="resume-section-content">
							    <ul class="list-unstyled resume-lang-list">
								    <li class="mb-2"><span class="resume-lang-name font-weight-bold">English</span> <small class="text-muted font-weight-normal">(Native)</small></li>
								    <li class="mb-2 align-middle"><span class="resume-lang-name font-weight-bold">French</span> <small class="text-muted font-weight-normal">(Professional)</small></li>
								    <li><span class="resume-lang-name font-weight-bold">Spanish</span> <small class="text-muted font-weight-normal">(Professional)</small></li>
							    </ul>
						    </div>
					    </section> --><!--//language-section-->
					    <section class="resume-section interests-section mb-5">
						    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">My hobby</h2>
						    <div class="resume-section-content">
							    <p>{{$getCV[0]['your_hobby']}}</p>
						    </div>
					    </section><!--//interests-section-->
					    
				    </div>
			    </div><!--//row-->
		    </div><!--//resume-body-->
		    @else
			   <h3 style="color:red; text-align: center; padding: 100px;">Dont have CV ! , click <a href="{{url('dashboard/u/cv/create')}}">Here</a> to create CV </h3>
		    @endif
		    
	    </div>
    </article>  
    @endsection