<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV extends Model
{
    //
    protected $table = "cv_student";
    
    protected $fillable = [
        'working_exp', 'personal_skill','education','project_participation','activities','goal_career','certificate', 'your_hobby','fullname','dayofbirth','category_id','user_id','update_at','create_at',
    ];
    public static function scopeSearch($query, $searchTerm)
    {
        return $query->where('personal_skill', 'like', '%' .$searchTerm. '%');
    }
}
