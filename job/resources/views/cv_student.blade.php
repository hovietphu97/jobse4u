@extends('layouts.theme')
 @section('content')
<div class="blog-single-page bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                    @if(empty($getCV))
                <div class="blog-single-title mt-5">
                    <h1>Create CV</h1>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-11 offset-md-1">
                <div class="blog-single-content pt-3 pb-5">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form method="POST" action="">
                        @csrf
                        <!-- <input name="personal_skill[]" type="text" style="display: none;" value="" class="total_skill"> -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Fullname</label>
                                    <div class="col-md-6">
                                     <input id="fullname" type="text" class="form-control" name="fullname" value="">
                                    {!! e_form_error('fullname', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Date of birth</label>
                                    <div class="col-md-6">
                                        <input id="dayofbirth" type="date" class="form-control" name="dayofbirth" value="">
                                        {!! e_form_error('dayofbirth', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Education</label>
                                    <div class="col-md-6">
                                        <textarea id="education" type="text" class="form-control" name="education" value=""></textarea>
                                        {!! e_form_error('education', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Work Experience</label>
                                    <div class="col-md-6">
                                        <textarea id="working_exp" type="text" class="form-control" name="working_exp" value=""></textarea>
                                        {!! e_form_error('working_exp', $errors) !!}
                                    </div>
                                </div>
                                <!-- <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Personal Skill</label>
                                    <div class="col-md-6">
                                        <input id="personal_skill" type="text" class="form-control" name="personal_skill" value="">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Project participation</label>
                                    <div class="col-md-6">
                                        <textarea id="project_participation" type="text" class="form-control" name="project_participation" value=""></textarea>
                                        {!! e_form_error('project_participation', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Activities</label>
                                    <div class="col-md-6">
                                        <textarea id="activities" type="text" class="form-control" name="activities" value=""></textarea>
                                        {!! e_form_error('activities', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Goal Career</label>
                                    <div class="col-md-6">
                                         <textarea id="goal_career" type="text" class="form-control" name="goal_career" value=""></textarea>
                                        {!! e_form_error('goal_career', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Certificate</label>
                                    <div class="col-md-6">
                                        <textarea id="certificate" type="text" class="form-control" name="certificate" value=""></textarea>
                                        {!! e_form_error('certificate', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">My Hobby</label>
                                    <div class="col-md-6">
                                        <textarea id="your_hobby" type="text" class="form-control" name="your_hobby" value=""></textarea>
                                        {!! e_form_error('your_hobby', $errors) !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input id="user_id" type="hidden" class="form-control" name="user_id" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 control-label">Category</label>
                                    <div class="col-md-6">
                                        <select name="category_id" class="form-control category">
                                            <option value="">--Select category--</option>
                                            @foreach($getCategory as $value)
                                                <option value="{{$value['id']}}">{{$value['category_name']}}</option>
                                            @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <table id="skill" class=" table order-list" style="display: none;">
                                    <thead>
                                        <tr>
                                            <td>Your skills</td>
                                            <td>progress</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input style="width: 300px;" type="text" class="form-control language" name="language[]" />
                                            </td>
                                            <td>
                                                <select style="padding:0;" class="form-control" class="progress" name="progress[]">
                                                    <option value = "10">10%</option>
                                                    <option value = "20">20%</option>
                                                    <option value = "30">30%</option>
                                                    <option value = "40">40%</option>
                                                    <option value = "50">50%</option>
                                                    <option value = "60">60%</option>
                                                    <option value = "70">70%</option>
                                                    <option value = "80">80%</option>
                                                    <option value = "90">90%</option>
                                                    <option value = "100">100%</option>
                                                </select>
                                            </td>
                                            <th class="col-sm-2">
                                                <a class="deleteRow"></a>
                                            </th>
                                        </tr>
                                        
                                    </tbody>
                                    
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" style="text-align: left;">
                                                <input type="button" class="btn btn-lg btn-block" style="background-color: #38c172;" id="addrow" value="Add Row" />
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button style="padding: 10px 20px" type="submit" class="btn btn-success">
                                    Create
                                </button>
                            </div>
                        </div>
                    </div>
                    
                </form>
                                        @else
                            <h3 style="text-align: center; color: red; font-weight: bold; margin: 200px 0;">CV has been created !!! Please click <a href="{{url('/dashboard/u/cv/view/'.Auth::user()->id)}}">HERE</a> to see your CV </h3>
                        @endif



            </div>
        </div>

    </div>
</div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type='text/javascript'>
    $(document).ready(function() {
        // $('form').on('submit', function(){
        //     var json = html2json();
        //     console.log($('input.total_skill').val())
        //     //console.log(json);
        //     return false;
        // })
      

        $('.category').on('change', function() {
             
            var category = $('.category').val();
            if(category != '') {
                $('table#skill').show();
            } else {
                $('table#skill').hide();
            }
        })
        var counter = 0;

        $("#addrow").on("click", function() {
            var newRow = $("<tr>");
            var cols = "";

            cols += '<td><input style="width: 300px;" type="text" class="form-control" name="language[]"/></td>';
            cols += '<td><select style="padding:0;" class="form-control" name="progress[]">' +
                '<option value = "10">10%</option>' +
                '<option value = "20">20%</option>' +
                '<option value = "30">30%</option>' +
                '<option value = "40">40%</option>' +
                '<option value = "50">50%</option>' +
                '<option value = "60">60%</option>' +
                '<option value = "70">70%</option>' +
                '<option value = "80">80%</option>' +
                '<option value = "90">90%</option>' +
                '<option value = "100">100%</option>' +
                '</select></td>';

            cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
            newRow.append(cols);
            $("table.order-list").append(newRow);
            counter++;
        });



        $("table.order-list").on("click", ".ibtnDel", function(event) {
            $(this).closest("tr").remove();
            counter -= 1
        });


    });


    function html2json() {   
        // Loop through grabbing everything
        var myRows = [];
        var $headers = $("table#skill");
        var $rows = $("table#skill tbody").each(function(index) {
            $cells = $(this).find("tr");
            myRows[index] = {};
            $cells.each(function(cellIndex) {
                //myRows[index][$($headers[cellIndex]).html()] = $(this).html();
                myRows[cellIndex] = [$(this).find('input').val(), $(this).find('select').val()];
            });    
        });
        // Let's put this in the object like you want and convert to JSON (Note: jQuery will also do this for you on the Ajax request)
        // var myObj = {};
        // myObj = myRows);
        console.log(myRows);
        $('input.total_skill').val(myRows);
        
        // alert(JSON.stringify(myObj));​
    }
    function calculateRow(row) {
        var price = +row.find('input[name^="price"]').val();

    }

    function calculateGrandTotal() {
        var grandTotal = 0;
        $("table.order-list").find('input[name^="price"]').each(function() {
            grandTotal += +$(this).val();
        });
        $("#grandtotal").text(grandTotal.toFixed(2));
    }
</script>
@endsection