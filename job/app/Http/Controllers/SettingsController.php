<?php

namespace App\Http\Controllers;

use App\Option;
use App\Pricing;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function GeneralSettings(){
        $title = trans('app.general_settings');
        return view('admin.settings-general', compact('title'));
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request) {
        $inputs = array_except($request->input(), ['_token']);
        foreach($inputs as $key => $value) {
            $option = Option::firstOrCreate(['option_key' => $key]);
            $option->option_value = $value;
            $option->save();
        }
        //check is request comes via ajax?
        if ($request->ajax()){
            return ['success'=>1, 'msg'=>trans('app.settings_saved_msg')];
        }
        return redirect()->back()->with('success', trans('app.settings_saved_msg'));
    }


}
